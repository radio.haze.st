#!/usr/bin/perl -w

$template_file = "/home/ivan/radio.haze.st/template.html";

use CGI;
use HTML::Table;
use Text::Template;

%type2color = (
  'upcoming'    => '999966',
  'live'        => 'ffffcc',
  'rebroadcast' => 'ffcc99',
);

%shows = (
  'high_jinx' => {
    'name'  => 'High Jinx',
    'day'   => 'sunday',
    'start' => '19',
    'len'   => '3',
    'type'  => 'upcoming',
    'startdate' => 'May 6th',
  },
  'high_jinx_rebroadcast' => {
    'name'  => 'High Jinx',
    'day'   => 'monday',
    'start' => '13',
    'len'   => '3',
    'type'  => 'upcoming',
    'startdate' => 'May 7th',
  },
  'dub_monday' => {
    'name'  => 'Dub Monday',
    'day'   => 'monday',
    'start' => '19.5',
    'len'   => '2',
    'url'   => 'http://www.voyager.org/sets',
    'type'  => 'live',
  },
  'dub_monday_rebroadcast' => {
    'name'  => 'Dub Monday',
    'day'   => 'tuesday',
    'start' => '13',
    'len'   => '2',
    'url'   => 'http://www.voyager.org/sets',
    'type'  => 'rebroadcast',
  },
  'organized_chaos' => {
    #'name'  => 'Organized<BR>Chaos',
    'name'  => 'Organized Chaos',
    'day'   => 'wednesday',
    'start' => '20',
    'len'   => '3',
    'url'   => 'http://www.angelfire.com/rant/burf/oc.html',
    'type'  => 'live',
  },
  'organized_chaos_rebroadcast' => {
    #'name'  => 'Organized<BR>Chaos',
    'name'  => 'Organized Chaos',
    'day'   => 'thursday',
    'start' => '13',
    'len'   => '3',
    'url'   => 'http://www.angelfire.com/rant/burf/oc.html',
    'type'  => 'rebroadcast',
  },
  'echo_communication_system' => {
    #'name'  => 'Echo<BR>Communication<BR>System',
    #'name'  => 'Echo Communication System',
    'name'  => 'Echo Communication System',
    'day'   => 'thursday',
    'start' => '20',
    'len'   => '3',
    'url'   => 'http://www.theoverworld.com/echo/',
    'type'  => 'live',
  },
  'echo_communication_system__rebroadcast' => {
    #'name'  => 'Echo<BR>Communication<BR>System',
    #'name'  => 'Echo Communication System',
    'name'  => 'Echo Communication System',
    'day'   => 'friday',
    'start' => '13',
    'len'   => '3',
    'url'   => 'http://www.theoverworld.com/echo/',
    'type'  => 'rebroadcast',
  },
  'sub_bass_sessions' => {
    'name'  => 'Sub-Bass Sessions',
    'day'   => 'tuesday',
    'start' => '20',
    'len'   => '2',
    'type'  => 'live',
  },
  'sub_bass_sessions_rebroadcast' => {
    'name'  => 'Sub-Bass Sessions',
    'day'   => 'wednesday',
    'start' => '13',
    'len'   => '2',
    'type'  => 'rebroadcast',
  },
);

@times = qw( midnight 1am 2am 3am 4am 5am 6am 7am 8am 9am 10am 11am noon 1pm
             2pm 3pm 4pm 5pm 6pm 7pm 8pm 9pm 10pm 11pm );
$increment = 0.5;
$showstart = 12;

$cgi = new CGI;
if ( defined($cgi->param('showstart'))
     && $cgi->param('showstart') =~ /^(\d+)$/ ) {
  die unless $1 < 24;
  $showstart = $1;
}

@showtimes = @times[$showstart..23];

$template = new Text::Template ( TYPE => 'FILE', SOURCE => $template_file )
  or die "Can't create template for $template_file: $Text::Template::ERROR";

my $text = $template->fill_in()
  or die "Can't fill in template for $template_file: $Text::Template::ERROR";

print $cgi->header, $text;

# subroutines for the template

sub table {

  my $rows = 1 + scalar(@showtimes)*(1/$increment);
  #$rows += 10;
  my $table = new HTML::Table($rows+20,8+2);
#  $table->setBorder(0);
  $table->setCellSpacing('0');
  $table->setCellPadding('1');

  my $daycol=2;
  foreach my $day ( map "${_}day", qw( Sun Mon Tues Wednes Thurs Fri Satur ) ) {
    $table->setCell(1, $daycol, "<B>${day}</B>" );
    $table->setCellAlign(1, $daycol, 'CENTER');

    my @dayshows =
      sort { $shows{$a}{'start'} <=> $shows{$b}{'start'} }
      grep { lc($shows{$_}{day}) eq lc($day) } keys %shows;
    my $pshowrow = 2;
    foreach my $show ( @dayshows ) {

      my $text = "<B>$shows{$show}{name}</B>";
      $text = qq(<A HREF="$shows{$show}{url}">$text</A>)
        if exists $shows{$show}{url};
      if ( $shows{$show}{type} eq 'upcoming' ) {
        $text .= qq(<BR><FONT SIZE="-1">starts $shows{$show}{startdate}</FONT>);
        #$text .= '<BR><FONT SIZE="-1">starts&nbsp;May&nbsp;Xth</FONT>';
      }
      if ( $shows{$show}{type} eq 'live' ) {
        $text .= "<BR><B>LIVE</B> ";
      }
      if ( $shows{$show}{type} eq 'rebroadcast' ) {
        #$text .= "<BR><B>rebroadcast</B> ";
        $text .= qq(<BR><FONT SIZE="-1">rebroadcast</FONT> );
      }
      if ( $shows{$show}{type} =~ /^(live|rebroadcast)$/ ) {
        my $start = $shows{$show}{start};
        my $stext = $times[ $start ];
        $stext =~ s/([0-9])([ap]m)/$1:30$2/ if $start != int($start);
        my $end = $shows{$show}{start} + $shows{$show}{len};
        my $etext = $times[$end];
        $etext =~ s/([0-9])([ap]m)/$1:30$2/ if $end != int($end);
        #$text .= "<B>$stext-$etext</B>";
        $text .= qq(<FONT SIZE="-1">$stext-$etext</FONT>);
      }

      my $showrow = 2 + (1/$increment) * ( $shows{$show}{start} - $showstart);

      if ( $pshowrow < $showrow ) {
        #$table->setCell($pshowrow, $daycol, '<B>OFF THE AIR</B>'. "($showrow/$pshowrow) ".($showrow-$pshowrow));
        $table->setCell($pshowrow, $daycol, '<B>OFF THE AIR</B>');
        $table->setCellAlign($pshowrow, $daycol, 'CENTER');
        $table->setCellRowSpan( $pshowrow, $daycol, $showrow-$pshowrow );
        #$table->setCellRowSpan( $pshowrow, $daycol, 1 );
        $table->setCellBGColor( $pshowrow, $daycol, '89201b' );
      }
      $table->setCell($showrow, $daycol, $text );
      $table->setCellAlign($showrow, $daycol, 'CENTER');
      $table->setCellBGColor($showrow, $daycol,
                             $type2color{$shows{$show}{type}});
      my $span = $shows{$show}{len} * (1/$increment);
      $table->setCellRowSpan($showrow, $daycol, $span );

      $pshowrow=$showrow+$span;
    }

    if ( $pshowrow < $rows+1 ) {
      #$table->setCell($pshowrow, $daycol, '<B>OFF THE AIR</B>'. "($rows+1/$pshowrow) ".($rows+1-$pshowrow));
      $table->setCell($pshowrow, $daycol, '<B>OFF THE AIR</B>');
      $table->setCellAlign($pshowrow, $daycol, 'CENTER');
      $table->setCellRowSpan( $pshowrow, $daycol, $rows+1-$pshowrow );
      #$table->setCellRowSpan( $pshowrow, $daycol, 2 );
      $table->setCellBGColor( $pshowrow, $daycol, '89201b' );
    }

    $daycol++;
  }

  my $timerow = 2;
  foreach my $time ( @showtimes ) {
    my $text = $time;
    $text = qq(<FONT SIZE="-1">$text</FONT>) if length($text) > 4;
    $table->setCell( $timerow, 1, "<B>$text</B>" );
    $table->setCellAlign( $timerow, 1, 'CENTER' );
    $table->setCellRowSpan( $timerow, 1, 1/$increment );
    #$table->setCell( $timerow+1, 1, "<B>$text:30</B>" );
#    $table->setCell( $timerow+1, 1, "&nbsp;" );
#    $table->setCellAlign( $timerow+1, 1, 'CENTER' );
    $timerow += 1/$increment;
  }

  local $^W=0;
  my $html = $table->getTable;
  $html =~ s/<! spanned cell\)>//g;
  return $html;
}


